#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include "ros/ros.h"
#include "std_msgs/Float32MultiArray.h"

// #include <Eigen/Dense>


#include <iostream>
#include <vector>
#include <math.h>
#include <array>

#define DEBUG -1

class PIDController{

private:

    // const float Kp_ = 10.0f;
    // const float Ki_ = 0.2f;
    // const float Kd_ = 3.0f;

    const float Kp_;
    const float Ki_;
    const float Kd_;
    const float Kp_extern_;
    

    float internalRef;
    // float internalMeasure = 0.0f;
    float accum_error_internal_ = 0.0f;
    float last_error_ = 0.0f;
    // float ref_ = 0.0f;

    bool has_saturation_ = false;
    float saturation_high_value_;
    float saturation_low_value_;

    float bias_ = 0.0f;

private:

     float internalPID(float measure, float ref , float dt){
        float error = measure - ref;
        float d_error = (error -last_error_)/dt; 
        float output  = Kp_ * error + Ki_ * accum_error_internal_ + Kd_ * d_error;
        
        // if (last_error_ * last_error_ < 0){
        //     accum_error_internal_ =0;
        // }else{

            accum_error_internal_ += error;
        // }
        last_error_ = error;
        return output;
     }

public:
    
    // PIDController(){};
    PIDController(float Kp,float Ki,float Kd,float Kp_e)
        :Kp_(Kp),Ki_(Ki),Kd_(Kd),Kp_extern_(Kp_e){}

    

    float computePID(float measure , float ref,float dt){
        float output;
        
        static float last_internalMeasure = 0.0f;
        static float internalMeasure = (measure - last_internalMeasure)/dt; 
        
        internalMeasure = 0.90 * internalMeasure + 0.1 * (measure - last_internalMeasure)/dt;
        // std::cout << "internalMeasure "<< internalMeasure <<std::endl;
        last_internalMeasure = measure;

        internalRef = Kp_extern_* (measure - ref);
        // std::cout << "internalRef " << internalRef << std::endl;
        output = -internalPID(internalMeasure,internalRef,dt); 
        
        // std::cout << "output " << output << std::endl;
        // std::cout << "internal error " << internalMeasure- internalRef << std::endl;
        

        output += bias_;

        // Saturate output
        if (has_saturation_){
            if (output > saturation_high_value_) output = saturation_high_value_;     
            else if (output < saturation_low_value_) output = saturation_low_value_;     
        }

        return output;
    
    };

    
    void setBias(float bias){ bias_ = bias;}
    void setCenteredBias(){ 
        if (has_saturation_){
         bias_ = (saturation_high_value_ - saturation_low_value_)/2.0f;   
        } else {
            bias_ = 0.0f;
            std::cout << "Controller does not have saturations" << std::endl;
        }
    }

    
    void setSaturation(float low,float high) {
        has_saturation_ = true;
        saturation_high_value_ = high; 
        saturation_low_value_ = low; 
    }

    void freeSaturation(){has_saturation_ = false;}
    
};


#endif