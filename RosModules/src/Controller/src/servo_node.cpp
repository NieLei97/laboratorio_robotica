#include "ros/ros.h"
#include "std_msgs/Float32.h"


#include <iostream>

#include "controller.hpp"

#define MAX_TORQUE 20000
#define MAX_SERVO_ANGLE 45 //deg

float servo_position = 0.0f;
float angle_measure = 0.0f;
std_msgs::Float32 torque;

void CallbackAngleMeasure(const std_msgs::Float32& msg){
    angle_measure = msg.data;
}

void CallbackServoMeasure(const std_msgs::Float32& msg){
    float signal = msg.data;
    servo_position = signal - 255.0f/2.0f;
    
    if (servo_position > MAX_SERVO_ANGLE) servo_position = MAX_SERVO_ANGLE;
    else if (servo_position < -MAX_SERVO_ANGLE) servo_position = -MAX_SERVO_ANGLE;
    
    // std::cout <<"Servo Position = " << servo_position << std::endl;
} 


int main(int argc, char **argv)
{
    ros::init(argc, argv,"Servo_node");
    ros::NodeHandle n;

    ros::Subscriber sub_angle = n.subscribe("/angle", 1, &CallbackAngleMeasure);
    ros::Subscriber sub_servo = n.subscribe("/servo", 1, &CallbackServoMeasure);
    ros::Publisher pub_torque = n.advertise<std_msgs::Float32>("/control", 1);


    // PIDController servo_controller(60.0f,0.05f,30.0,0.01f);
    PIDController servo_controller(40.0f,0.05f,0.001f,2.0f);
    servo_controller.setSaturation(-MAX_TORQUE,MAX_TORQUE);

    auto t0 = ros::Time::now(); 
    
    ros::Rate rate(100);
    
    while(ros::ok())
    {
        //updating all the ros msgs
        ros::spinOnce();
        // running the localizer

        // std::cout << "angle Measured = "<< angle_measure << "  servo_position = " << servo_position << std::endl;
        auto d_t = t0 - ros::Time::now();
        auto delta_t = d_t.toSec();

        torque.data = servo_controller.computePID(angle_measure,servo_position,delta_t);
        

        // std::cout <<"torque = "<< torque.data << std::endl; 
        pub_torque.publish(torque);
        
        t0 = ros::Time::now();
        
        rate.sleep();
    }

    return 0;
}
    