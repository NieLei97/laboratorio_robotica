#include "ros/ros.h"
#include "std_msgs/Float32.h"


#include <iostream>

#include "controller.hpp"
   


float laser_measure = 0.0f;
float dist_ref = 0.0f;

std_msgs::Float32 servo_position;

void CallbackDistRef(const std_msgs::Float32& msg){
    dist_ref = msg.data;
}

void CallbackLaserMeasure(const std_msgs::Float32& msg){
    laser_measure = msg.data;
    if (msg.data >= 50) laser_measure = 0.0f;  
    laser_measure = - (laser_measure -  25);
    // std::cout <<"Laser Measure = " << laser_measure << std::endl;
} 


   
int main(int argc, char **argv)
{
    ros::init(argc, argv,"Controller_node");
    ros::NodeHandle n;
    ros::Subscriber sub_laser = n.subscribe("/laser", 1, &CallbackLaserMeasure);
    ros::Subscriber sub_dist = n.subscribe("/dist_ref", 1, &CallbackDistRef);
    ros::Publisher pub_servo = n.advertise<std_msgs::Float32>("/servo", 1);


    PIDController ball_controller(1.55f,0.000002f,0.1f,0.8f);
    ball_controller.setSaturation(0,255.0f);
    ball_controller.setCenteredBias();

    auto t0 = ros::Time::now(); 
    ros::Rate rate(100);
    while(ros::ok())
    {
        //updating all the ros msgs
        ros::spinOnce();
        // running the localizer

         auto d_t = t0 - ros::Time::now();
        auto delta_t = d_t.toSec();

        servo_position.data = ball_controller.computePID(laser_measure,dist_ref,delta_t);
        pub_servo.publish(servo_position);
        t0 = ros::Time::now();

        
        rate.sleep();
    }

    return 0;
}
    